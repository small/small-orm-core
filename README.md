# small-orm-core

Small ORM is a small php ORM.

This package is the core of Small ORM.

- A Symfony bundle for Small ORM is available here : https://framagit.org/small/small-orm-bundle
- A Swoft package for Small ORM is available here (not maintained anymore) : https://github.com/sebk69/small-orm-swoft

## Documentation

The documentation is available here : https://framagit.org/small/small-orm-doc

## Install

Require the package with composer:
```
composer require small/orm-core
```