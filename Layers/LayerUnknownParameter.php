<?php
/**
 * This file is a part of sebk/small-orm-core
 * Copyright 2021-2023 - Sébastien Kus
 * Under GNU GPL V3 licence
 */

namespace Sebk\SmallOrmCore\Layers;


class LayerUnknownParameter extends \exception
{

}
